//[Section] JavaScript Synchronous vs Asynchronous
// JavaScript is by default synchronous means that only one statement can be executed at a time

console.log("hello world");
//conosole.log("hello world");
console.log("hello world");

//code blocking

//Asynchronous means that we can proceed to execute other statements, while time consuming codes are running in the background

//The Fetch API allows you to asynchronously request for a resource (data)
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value
	/* Syntax
		fetch('URL')
		.then((response) => {})
	*/

//[CHECK THE STATUS OF THE REQUEST]
//By using the .then method we can now check for the status of the promise


fetch('https://jsonplaceholder.typicode.com/posts')
.then( response => console.log(response.status));
//The "fetch" method will return a "promise" that resolves to a "response" object
//The .then method captures the "Response" object and returns another "promise" whixh will eventually be "resolved" or "rejected"

//[RETRIEVE CONTENTS/DATA FROM THE RESPONSE OBJECT]
fetch('https://jsonplaceholder.typicode.com/posts')
.then((response)=> response.json())

//using multiple "then" method creates a "promise chain"
.then((json)=> console.log(json))


//[Section] Create a function that will demonstrate using 'async' and 'await' keywords

//The 'async' and 'await' keywords is another approach that can be used to achieve an asynchronous code
//Used in functions to indicate which portions of code should be waited for
//Creates an synchronous function

async function fetchData(){
	//waits for the 'fetch' method to complete then stores the value in the result variable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	//Result returned by fetch is a "promise"
	console.log(result);

	//The returned "response" is an object
	console.log(typeof result);
	
	//We can not access the content of the "Response directly by a accessing its body property"
	console.log(result.body)

	//converts the data from the "response" object
	let json = await result.json();
	console.log(json);

};

fetchData();

//[Section] RETRIEVE A SPECIFIC POST

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response)=>response.json())
.then((json)=>console.log(json))

//[Section] CREATE POST
/* Syntax
	fetch('URL', options)
	.then((response)=>{})
	.then((response)=>{})
*/

fetch('https://jsonplaceholder.typicode.com/posts',{
		//Sets the method of the "Request" objects to POST
		method: 'POST',

		//Specifed that the content will be in a JSON structure
		headers : {
			'Content-type':'application/json'
		},
		body: JSON.stringify({
			title: 'New Post',
			body: 'Hello world!',
			userId: 1
		})
})
.then((response)=> response.json())
.then((json)=> console.log(json));

//[Section] UPDATE A POST USING PUT METHOD

fetch('https://jsonplaceholder.typicode.com/posts/1',{
		method: 'PUT',
		headers: {
			'Content-type':'application/json',
		},
		body: JSON.stringify({
			id:1,
			title: 'Updated post',
			body: 'Hello again',
			userId: 1
		})
	})
	.then((response)=> response.json())
	.then((json)=> console.log(json));

//[Section] UPDATE A POST USING PATCH METHOD

	//Patch is used to update the whole object
	//Put is used to update a single property

	fetch('https://jsonplaceholder.typicode.com/posts/1',{
			method: 'PATCH',
			headers: {
				'Content-type':'application/json',
			},
			body: JSON.stringify({
				
				title: 'Corrected post',
				
			})
		})
		.then((response)=> response.json())
		.then((json)=> console.log(json));

//[Section] DELETE A POST METHOD

fetch('https://jsonplaceholder.typicode.com/posts/1',{
			method: 'DELETE'
			
		})
		
